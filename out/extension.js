"use strict";var q=Object.create;var b=Object.defineProperty;var T=Object.getOwnPropertyDescriptor;var _=Object.getOwnPropertyNames;var z=Object.getPrototypeOf,P=Object.prototype.hasOwnProperty;var j=(e,n)=>{for(var s in n)b(e,s,{get:n[s],enumerable:!0})},y=(e,n,s,d)=>{if(n&&typeof n=="object"||typeof n=="function")for(let o of _(n))!P.call(e,o)&&o!==s&&b(e,o,{get:()=>n[o],enumerable:!(d=T(n,o))||d.enumerable});return e};var x=(e,n,s)=>(s=e!=null?q(z(e)):{},y(n||!e||!e.__esModule?b(s,"default",{value:e,enumerable:!0}):s,e)),M=e=>y(b({},"__esModule",{value:!0}),e);var Y={};j(Y,{activate:()=>H,deactivate:()=>G});module.exports=M(Y);var a=x(require("vscode")),i=require("fs"),c=x(require("path"));var S=`(function () {\r
    const bodyElement = document.querySelector('body');\r
\r
    let Workbench = {\r
        TitleBar: undefined,\r
        Banner: undefined,\r
        ActivityBar: undefined,\r
        SideBar: undefined,\r
        Editor: undefined,\r
        AuxiliaryBar: undefined,\r
        BottomPanel: undefined,\r
        StatusBar: undefined,\r
\r
        Container: []\r
    };\r
\r
    const bootstrapObserver = new MutationObserver(WatchBootstrap);\r
\r
    function InitializeFluentUI() {\r
        QueryWorkbench();\r
        ApplyFluentStyling();\r
    }\r
\r
    function QueryWorkbench() {\r
        Workbench.TitleBar      = bodyElement.querySelector('#workbench\\\\.parts\\\\.titlebar')?.parentElement;\r
        Workbench.Banner        = bodyElement.querySelector('#workbench\\\\.parts\\\\.banner')?.parentElement;\r
        Workbench.ActivityBar   = bodyElement.querySelector('#workbench\\\\.parts\\\\.activitybar')?.parentElement;\r
        Workbench.SideBar       = bodyElement.querySelector('#workbench\\\\.parts\\\\.sidebar')?.parentElement;\r
        Workbench.Editor        = bodyElement.querySelector('#workbench\\\\.parts\\\\.editor')?.parentElement;\r
        Workbench.AuxiliaryBar  = bodyElement.querySelector('#workbench\\\\.parts\\\\.auxiliarybar')?.parentElement;\r
        Workbench.BottomPanel   = bodyElement.querySelector('#workbench\\\\.parts\\\\.panel')?.parentElement;\r
        Workbench.StatusBar     = bodyElement.querySelector('#workbench\\\\.parts\\\\.statusbar')?.parentElement;\r
\r
        Workbench.Container[0]  = Workbench.TitleBar?.parentElement;\r
        Workbench.Container[1]  = Workbench.Container[0]?.childNodes[2];\r
\r
        Workbench.Container[2]  = Workbench.AuxiliaryBar.parentElement;\r
        Workbench.Container[3]  = Workbench.Container[2]?.childNodes[2];\r
\r
        Workbench.Container[4]  = Workbench.Editor?.parentElement;\r
    }\r
\r
    function ApplyFluentStyling() {\r
        Workbench.Container[0].classList.add('fluent-container-0');\r
\r
        Workbench.TitleBar.classList.add('fluent-titlebar');\r
        Workbench.Banner.classList.add('fluent-banner');\r
        Workbench.Container[1].classList.add('fluent-container-1');\r
        Workbench.StatusBar.classList.add('fluent-statusbar');\r
\r
        Workbench.Container[2].classList.add('fluent-container-2');\r
\r
        Workbench.ActivityBar.classList.add('fluent-activitybar');\r
        Workbench.SideBar.classList.add('fluent-sidebar');\r
        Workbench.Container[3].classList.add('fluent-container-3');\r
        Workbench.AuxiliaryBar.classList.add('fluent-auxiliarybar');\r
\r
        Workbench.Container[4].classList.add('fluent-container-4');\r
\r
        Workbench.Editor.classList.add('fluent-editor');\r
        Workbench.BottomPanel.classList.add('fluent-bottompanel');\r
\r
        if (useAcrylic) { bodyElement.classList.add('acrylic'); }\r
        if (useShadows) {\r
            Workbench.SideBar.firstChild.classList.add('fluent-shadow');\r
            Workbench.BottomPanel.firstChild.classList.add('fluent-shadow');\r
        }\r
    }\r
\r
    function WatchBootstrap(_list, _observer) {\r
        for (let _mutation of _list) {\r
            if (_mutation.type === 'childList') {\r
                const vscToken = document.querySelector('.vscode-tokens-styles');\r
\r
                if (vscToken) {\r
                    _observer.disconnect();\r
                    ReportStatus('Bootstrap located, initializing...');\r
                    InitializeFluentUI();\r
                }\r
            }\r
        }\r
    }\r
\r
    function ReportStatus(_message) { console.log('[FluentUI] - ' + _message); }\r
\r
    bootstrapObserver.observe(bodyElement, { childList: true });\r
})();`;var B=`:root {\r
    --fluent-acrylic-blur: 8px;\r
\r
    --fluent-spacing-xsm: 4px;\r
    --fluent-spacing-sml: 8px;\r
    --fluent-spacing-mdm: 12px;\r
    --fluent-spacing-lrg: 16px;\r
\r
    --fluent-border-radius-sml: 2px;\r
    --fluent-border-radius-mdm: 4px;\r
    --fluent-border-radius-lrg: 6px;\r
    --fluent-border-radius-crl: 46px;\r
\r
    --fluent-border: 1px solid rgba(127, 127, 127, .08);\r
\r
    --fluent-shadow2-light: 0 0 2px rgba(0,0,0,0.12), 0 2px 4px rgba(0,0,0,0.14);\r
    --fluent-shadow2-dark: 0 0 2px rgba(0,0,0,0.24), 0 2px 4px rgba(0,0,0,0.28);\r
    --fluent-shadow16-light: 0 0 2px rgba(0,0,0,0.12), 0 8px 16px rgba(0,0,0,0.14);\r
    --fluent-shadow16-dark: 0 0 2px rgba(0,0,0,0.24), 0 8px 16px rgba(0,0,0,0.28);\r
}\r
\r
\r
\r
/*----------------------------------------------------------------\r
    fluent.utilities\r
----------------------------------------------------------------*/\r
.fluent-shadow::after {\r
    z-index: 255; position: absolute;\r
    content: '';\r
    box-sizing: border-box;\r
    top: 0; left: 0;\r
    width: 100%; height: 100%;\r
    pointer-events: none;\r
    box-shadow: var(--fluent-shadow2-light);\r
    border: var(--fluent-border);\r
}\r
\r
[class='vs-dark'] .fluent-shadow::after {\r
    box-shadow: var(--fluent-shadow2-dark);\r
}\r
\r
.fluent-container-0 { display: flex; flex-direction: column; }\r
.fluent-container-0 > div { position: relative !important; }\r
.fluent-container-1 { top: unset !important; height: auto !important; flex: 1 1 auto !important; }\r
\r
.fluent-container-4 { display: flex; flex-direction: column; }\r
\r
.separator-border { --separator-border: transparent !important; }\r
\r
\r
\r
/*----------------------------------------------------------------\r
    fluent-titlebar\r
        workbench.parts.titlebar\r
----------------------------------------------------------------*/\r
.fluent-titlebar { top: 0 !important; height: 32px !important; }\r
#workbench\\.parts\\.titlebar { height: 100% !important; border: unset !important; background-color: var(--vscode-editor-background) !important; }\r
\r
\r
\r
/*----------------------------------------------------------------\r
    fluent-banner\r
        workbench.parts.banner\r
----------------------------------------------------------------*/\r
.fluent-banner {\r
    box-sizing: border-box;\r
    z-index: 127 !important;\r
    position: absolute !important;\r
    top: 48px !important; left: 2rem !important;\r
    height: 32px !important; width: calc(100% - 4rem) !important;\r
    overflow: hidden;\r
\r
    border-radius: var(--fluent-border-radius-crl);\r
    box-shadow: var(--fluent-shadow16-light);\r
    border: var(--fluent-border);\r
}\r
\r
.fluent-banner div { display: flex; align-items: center; height: 100% }\r
\r
.fluent-banner::after {\r
    z-index: -1;\r
    position: absolute;\r
    content: '';\r
    top: 0; left: 0;\r
    width: 100%; height: 100%;\r
    background-color: var(--vscode-panel-background);\r
}\r
\r
.acrylic .fluent-banner { backdrop-filter: blur(var(--fluent-acrylic-blur)); }\r
.acrylic .fluent-banner::after { background-color: transparent; }\r
\r
[class='vs-dark'] .fluent-banner { box-shadow: var(--fluent-shadow16-dark); }\r
\r
\r
\r
/*----------------------------------------------------------------\r
    fluent-activitybar\r
        workbench.parts.activitybar \r
----------------------------------------------------------------*/\r
#workbench\\.parts\\.activitybar { background-color: var(--vscode-editor-background) !important; }\r
#workbench\\.parts\\.activitybar.bordered { border-color: var(--vscode-editor-background) !important; }\r
\r
\r
\r
/*----------------------------------------------------------------\r
    fluent-sidebar\r
        workbench.parts.sidebar\r
----------------------------------------------------------------*/\r
#workbench\\.parts\\.sidebar { \r
    display: flex; flex-direction: column; \r
    height: 100% !important; \r
    border: none !important;\r
    padding-bottom: var(--fluent-spacing-sml);\r
    background-color: var(--vscode-editor-background) !important;\r
}\r
\r
#workbench\\.parts\\.sidebar::after { \r
    border-radius: var(--fluent-border-radius-lrg);\r
    height: calc(100% - var(--fluent-spacing-sml)); \r
}\r
\r
#workbench\\.parts\\.sidebar > div { position: relative !important; border: var(--fluent-border); }\r
\r
#workbench\\.parts\\.sidebar > .title { \r
    box-sizing: border-box;\r
    border-top-left-radius: var(--fluent-border-radius-lrg);\r
    border-top-right-radius: var(--fluent-border-radius-lrg);\r
    background-color: var(--vscode-sideBar-background) !important;\r
}\r
\r
#workbench\\.parts\\.sidebar > .content {\r
    overflow: hidden; flex: 1 1 auto;\r
    height: auto !important; width: 100% !important;\r
    border-bottom-left-radius: var(--fluent-border-radius-lrg);\r
    border-bottom-right-radius: var(--fluent-border-radius-lrg);\r
    background-color: var(--vscode-sideBar-background) !important;\r
}\r
\r
/*  Search-view fixes       */\r
#workbench\\.view\\.search [class="replace-container"] { display: flex; }\r
#workbench\\.view\\.search [class="replace-container"] .replace-input { flex: 1 1 auto; }\r
#workbench\\.view\\.search [class="replace-container"] .replace-input .input { width: auto !important; }\r
\r
/*  Welcome-biew fixes      */\r
#workbench\\.parts\\.sidebar .welcome-view-content { width: 100% !important; padding-inline: var(--fluent-spacing-lrg); }\r
\r
\r
\r
/*----------------------------------------------------------------\r
    fluent-editor\r
        workbench.parts.editor\r
----------------------------------------------------------------*/\r
.fluent-editor { position: relative !important; }\r
\r
#workbench\\.parts\\.editor {\r
    position: relative !important;\r
    width: 100% !important; height: 100% !important; \r
}\r
\r
#workbench\\.parts\\.editor > div { position: relative !important; width: 100% !important; height: 100% !important; }\r
#workbench\\.parts\\.editor .editor-group-container { display: flex; flex-direction: column; }\r
\r
\r
#workbench\\.parts\\.editor .title.tabs { \r
    position: relative;\r
    overflow: visible !important;\r
    margin-inline: var(--fluent-spacing-sml);\r
    background-color: transparent !important;\r
}\r
\r
#workbench\\.parts\\.editor .tabs-breadcrumbs {\r
    position: relative;\r
    box-sizing: border-box;\r
    border-top-left-radius: var(--fluent-border-radius-lrg);\r
    border-top-right-radius: var(--fluent-border-radius-lrg);\r
    border: var(--fluent-border) !important;\r
    box-shadow: var(--fluent-shadow2-dark);\r
}\r
\r
#workbench\\.parts\\.editor .title.tabs .tab { --tab-border-top-color: transparent !important; background-color: transparent !important; border: unset !important; }\r
#workbench\\.parts\\.editor .editor-actions { outline: none !important; }\r
#workbench\\.parts\\.editor .tabs-container { padding-left: var(--fluent-spacing-mdm); }\r
#workbench\\.parts\\.editor .tabs-and-actions-container { --tabs-border-bottom-color: transparent !important; }\r
#workbench\\.parts\\.editor .tab-border-bottom-container { display: none; }\r
\r
\r
#workbench\\.parts\\.editor .tabs .tab.active { \r
    background-color: var(--vscode-editor-background) !important; \r
    border-top-left-radius: var(--fluent-border-radius-lrg);\r
    border-top-right-radius: var(--fluent-border-radius-lrg);\r
    border: var(--fluent-border) !important;\r
    box-shadow: var(--fluent-shadow2-dark);\r
}\r
\r
#workbench\\.parts\\.editor .editor-container {\r
    box-sizing: border-box;\r
    position: relative; overflow: hidden; \r
    margin-inline: var(--fluent-spacing-sml); margin-bottom: var(--fluent-spacing-sml);\r
    width: unset !important; height: unset !important;\r
    border-bottom-left-radius: var(--fluent-border-radius-lrg);\r
    border-bottom-right-radius: var(--fluent-border-radius-lrg);\r
    border: var(--fluent-border) !important;\r
    box-shadow: var(--fluent-shadow2-dark);\r
}\r
\r
\r
\r
/*----------------------------------------------------------------\r
    fluent-auxiliarybar\r
        workbench.parts.auxilarybar\r
----------------------------------------------------------------*/\r
\r
\r
\r
/*----------------------------------------------------------------\r
    fluent-bottompanel\r
        workbench.parts.panel\r
----------------------------------------------------------------*/\r
.fluent-bottompanel { \r
    position: relative !important; \r
    height: auto !important;\r
    top: unset !important;\r
    flex: 1 1 auto;\r
}\r
\r
#workbench\\.parts\\.panel {\r
    position: relative !important;\r
    display: flex; \r
    flex-direction: column; \r
    justify-content: center;\r
    height: 100% !important;\r
    padding-inline: var(--fluent-spacing-sml);\r
    padding-bottom: var(--fluent-spacing-sml);\r
    background-color: var(--vscode-editor-background) !important;\r
}\r
\r
#workbench\\.parts\\.panel > div { position: relative !important; border: var(--fluent-border); }\r
\r
#workbench\\.parts\\.panel::after { \r
    border-radius: var(--fluent-border-radius-lrg);\r
    left: var(--fluent-spacing-sml);\r
    width: calc(100% - (var(--fluent-spacing-sml) * 2));\r
    height: calc(100% - var(--fluent-spacing-sml)); \r
}\r
\r
#workbench\\.parts\\.panel > .title { \r
    height: 32px !important;\r
    border-top: none;\r
    border-top-left-radius: var(--fluent-border-radius-lrg);\r
    border-top-right-radius: var(--fluent-border-radius-lrg);\r
    background-color: var(--vscode-sideBar-background) !important;\r
}\r
\r
#workbench\\.parts\\.panel .title .title-actions { height: 100% !important; }\r
\r
#workbench\\.parts\\.panel > .content {\r
    height: auto !important;\r
    width: 100% !important;\r
    flex: 1 1 auto;\r
    overflow: hidden;\r
    border-bottom-left-radius: var(--fluent-border-radius-lrg);\r
    border-bottom-right-radius: var(--fluent-border-radius-lrg);\r
    background-color: var(--vscode-sideBar-background) !important;\r
}\r
\r
/*----------------------------------------------------------------\r
    fluent-statusbar\r
        workbench.parts.statusbar \r
----------------------------------------------------------------*/\r
.fluent-statusbar { \r
    position: relative !important;\r
    justify-content: center;\r
    align-items: center;\r
    display: flex;\r
    top: unset !important;\r
    height: 24px !important;\r
}\r
\r
#workbench\\.parts\\.statusbar { \r
    --status-border-top-color: var(--vscode-statusBar-background) !important;\r
    height: 100% !important; \r
}`;function H(e){f("Initializing FluentUI");let n=c.dirname(require.main?.filename),s=c.join(n,"vs","code"),d=c.join(s,"electron-sandbox","workbench"),o=c.join(d,"workbench.html"),p={ADMIN:"Please run VSC with Administrator Priviliges in order to enable FluentUI",ENABLED:"FluentUI is now enabled, reloading VSC for the changes to take effect",DISABLED:"FluentUI is now disabled, reloading VSC for the changes to take effect"};async function m(){let t=Date.now();await E(t),await L(t)}async function g(){let t=await F(o);if(!t)return;let r=k(t);await W(r),await C()}async function I(){await g(),await m()}async function E(t){try{let r=await i.promises.readFile(o,"utf-8"),u=k(t);r=w(r),await i.promises.writeFile(u,r,"utf-8"),f(`Backup created: ${o}`)}catch(r){h(r),a.window.showInformationMessage(p.ADMIN)}}async function W(t){try{(0,i.existsSync)(t)&&(await i.promises.unlink(o),await i.promises.copyFile(t,o))}catch{a.window.showInformationMessage(p.ADMIN)}}async function C(){let t=await i.promises.readdir(d);for(let r in t)r.endsWith(".bak-fui")&&await i.promises.unlink(c.join(d,r))}async function F(t){try{let u=(await i.promises.readFile(t,"utf-8")).match(/<!-- FUI-ID ([0-9a-fA-F-]+) -->/);return 0}catch(r){h(r),a.window.showInformationMessage(`${r}`)}}function k(t){return c.join(d,`workbench.${t}.bak-fui`)}function w(t){return t=t.replace(/^.*(<!-- Fluent UI --><script src="fluent.js"><\/script><!-- Fluent UI -->).*\n?/gm,""),t=t.replace(/<!-- FUI-CSS-START -->[\s\S]*?<!-- FUI-CSS-END -->\n*/,""),t=t.replace(/<!-- FUI -->[\s\S]*?<!-- FUI -->\n*/,""),t=t.replace(/<!-- FUI-ID [\w-]+ -->\n*/g,""),t=t.replace(/<!-- meta-default -->/,`<meta http-equiv="Content-Security-Policy" content="default-src 'none'; img-src 'self' https: data: blob: vscode-remote-resource:; media-src 'self'; frame-src 'self' vscode-webview:; object-src 'self'; script-src 'self' 'unsafe-eval' blob:; style-src 'self' 'unsafe-inline'; connect-src 'self' https: ws:; font-src 'self' https: vscode-remote-resource:;">`),t=t.replace(/<!-- meta-require -->/,`<meta http-equiv="Content-Security-Policy" content="require-trusted-types-for 'script'; trusted-types amdLoader cellRendererEditorText defaultWorkerFactory diffEditorWidget stickyScrollViewLayer editorGhostText domLineBreaksComputer editorViewLayer diffReview dompurify notebookRenderer safeInnerHtml standaloneColorizer tokenizeToString;">`),t}async function L(t){let r=a.workspace.getConfiguration("fluentui").get("useAcrylic"),u=a.workspace.getConfiguration("fluentui").get("useShadows"),R=`let useAcrylic=${r};let useShadows=${u};`,l=await i.promises.readFile(o,"utf-8");l=w(l),l=l.replace(/<meta.*http-equiv="Content-Security-Policy".*content="default.*>/,"<!-- meta-default -->"),l=l.replace(/<meta.*http-equiv="Content-Security-Policy".*content="require.*>/,"<!-- meta-require -->"),l=l.replace(/(<\/html>)/,`<!-- FUI-ID ${t} -->
<!-- FUI-CSS-START -->
<style>${B}</style><script>${R}${S}<\/script><!-- FUI-CSS-END -->
</html>`);try{await i.promises.writeFile(o,l,"utf-8"),f("FluentUI Injected into Workbench!"),a.window.showInformationMessage(p.ENABLED)}catch(v){h(v),a.window.showInformationMessage("Error"+v)}}function f(t){console.log(`[FluentUI] - ${t}`)}function h(t){console.error(`[FluentUI] - ${t}`)}let A=a.commands.registerCommand("fluentui.install",m),U=a.commands.registerCommand("fluentui.uninstall",g),D=a.commands.registerCommand("fluentui.reinstall",I);e.subscriptions.push(A),e.subscriptions.push(U),e.subscriptions.push(D)}function G(){}0&&(module.exports={activate,deactivate});
//# sourceMappingURL=data:application/json;base64,ewogICJ2ZXJzaW9uIjogMywKICAic291cmNlcyI6IFsiLi4vc3JjL2V4dGVuc2lvbi50cyJdLAogICJzb3VyY2VzQ29udGVudCI6IFsiaW1wb3J0ICogYXMgdnNjb2RlIGZyb20gJ3ZzY29kZSc7XHJcblxyXG4vLyBcdEB0cy1pZ25vcmVcclxuaW1wb3J0IHsgZXhpc3RzU3luYywgcHJvbWlzZXMgfSBmcm9tICdmcyc7XHJcbmltcG9ydCAqIGFzIHBhdGggZnJvbSAncGF0aCdcclxuXHJcbi8vICBAdHMtaWdub3JlXHJcbmltcG9ydCAqIGFzIEZsdWVudFNjcmlwdCBmcm9tICcuL2luamVjdGlvbnMvZmx1ZW50U2NyaXB0LmpzJztcclxuXHJcbi8vICBAdHMtaWdub3JlXHJcbmltcG9ydCAqIGFzIEZsdWVudFN0eWxlIGZyb20gJy4vaW5qZWN0aW9ucy9mbHVlbnRTdHlsZS5jc3MnO1xyXG5cclxuLyoqXHJcbiAqIEBwYXJhbSB7dnNjb2RlLkV4dGVuc2lvbkNvbnRleHR9IGNvbnRleHRcclxuICovXHJcbmV4cG9ydCBmdW5jdGlvbiBhY3RpdmF0ZShjb250ZXh0OiB2c2NvZGUuRXh0ZW5zaW9uQ29udGV4dCkge1xyXG5cdFJlcG9ydFN0YXR1cygnSW5pdGlhbGl6aW5nIEZsdWVudFVJJyk7XHJcblxyXG5cdGNvbnN0IEFQUF9ESVJFQ1RPUlk6IHN0cmluZyA9IHBhdGguZGlybmFtZShyZXF1aXJlLm1haW4/LmZpbGVuYW1lIGFzIHN0cmluZyk7XHJcblx0Y29uc3QgQkFTRV9ESVJFQ1RPUlk6IHN0cmluZyA9IHBhdGguam9pbihBUFBfRElSRUNUT1JZLCAndnMnLCAnY29kZScpO1xyXG5cdGNvbnN0IFdPUktCRU5DSF9ESVJFQ1RPUlk6IHN0cmluZyA9IHBhdGguam9pbihCQVNFX0RJUkVDVE9SWSwgJ2VsZWN0cm9uLXNhbmRib3gnLCAnd29ya2JlbmNoJyk7XHJcblx0Y29uc3QgV09SS0JFTkNIX0ZJTEU6IHN0cmluZyA9IHBhdGguam9pbihXT1JLQkVOQ0hfRElSRUNUT1JZLCAnd29ya2JlbmNoLmh0bWwnKTtcclxuXHJcblx0Y29uc3QgTUVTU0FHRVMgPSB7XHJcblx0XHRBRE1JTjogJ1BsZWFzZSBydW4gVlNDIHdpdGggQWRtaW5pc3RyYXRvciBQcml2aWxpZ2VzIGluIG9yZGVyIHRvIGVuYWJsZSBGbHVlbnRVSScsXHJcblx0XHRFTkFCTEVEOiAnRmx1ZW50VUkgaXMgbm93IGVuYWJsZWQsIHJlbG9hZGluZyBWU0MgZm9yIHRoZSBjaGFuZ2VzIHRvIHRha2UgZWZmZWN0JyxcclxuXHRcdERJU0FCTEVEOiAnRmx1ZW50VUkgaXMgbm93IGRpc2FibGVkLCByZWxvYWRpbmcgVlNDIGZvciB0aGUgY2hhbmdlcyB0byB0YWtlIGVmZmVjdCdcclxuXHR9O1xyXG5cclxuXHQvKipcclxuXHQgKiBcdEluc3RhbGxzIHRoZSBXb3Jrc3BhY2UgbW9kaWZpY2F0aW9uXHJcblx0ICogXHJcblx0ICogXHRcdFRoaXMgaXMgZG9uZSBieSBmaXJzdCBnZW5lcmF0aW5nIGEgU2Vzc2lvbi1VVUlELCB0aGVuIGNyZWF0aW5nIGEgYmFja3VwIG9mIHRoZSBcclxuXHQgKiBcdFx0Y3VycmVudCB3b3Jrc3BhY2UgYW5kIHRoZW4gaW5qZWN0aW5nIHRoZSByZXF1aXJlcyBmaWxlcyBmb3IgYWRqdXN0bWVudHMuXHJcblx0ICovXHJcblx0YXN5bmMgZnVuY3Rpb24gSW5zdGFsbCgpIHtcclxuXHRcdGxldCBfc2Vzc2lvbklEID0gRGF0ZS5ub3coKTtcclxuXHRcdGF3YWl0IENyZWF0ZUJhY2t1cChfc2Vzc2lvbklEKTtcclxuXHRcdGF3YWl0IEluamVjdChfc2Vzc2lvbklEKTtcclxuXHR9XHJcblxyXG5cdC8qKlxyXG5cdCAqIFx0XHJcblx0ICogQHJldHVybnMgXHJcblx0ICovXHJcblx0YXN5bmMgZnVuY3Rpb24gVW5pbnN0YWxsKCkge1xyXG5cdFx0Y29uc3QgYmFja3VwSUQgPSBhd2FpdCBGaW5kQmFja3VwQnlVVUlEKFdPUktCRU5DSF9GSUxFKTtcclxuXHRcdGlmICghYmFja3VwSUQpIHJldHVybjtcclxuXHJcblx0XHRjb25zdCBiYWNrdXBQYXRoID0gR2V0QmFja3VwQnlVVUlEKGJhY2t1cElEKTtcclxuXHRcdGF3YWl0IFJlc3RvcmVCYWNrdXAoYmFja3VwUGF0aCk7XHJcblx0XHRhd2FpdCBSZW1vdmVCYWNrdXBGaWxlcygpO1xyXG5cdH1cclxuXHJcblx0YXN5bmMgZnVuY3Rpb24gUmVpbnN0YWxsKCkge1xyXG5cdFx0YXdhaXQgVW5pbnN0YWxsKCk7XHJcblx0XHRhd2FpdCBJbnN0YWxsKCk7XHJcblx0fVxyXG5cclxuXHQvKipcclxuXHQgKiBcdENyZWF0ZXMgYSBiYWNrdXAgb2YgdGhlIGN1cnJlbnQgd29ya3NwYWNlLlxyXG5cdCAqIFxyXG5cdCAqIFx0XHRUaGlzIGlzIGRvbmUgYnkgcmVhZGluZyB0aGUgY3VycmVudCB3b3JrYmVuY2ggZmlsZSwgc3RyaXBwaW5nL3Nhbml0aXppbmcgaXQgZnJvbSBcclxuXHQgKiBcdFx0YW55IGV4aXN0aW5nIGluamVjdGlvbnMgZG9uZSBieSBGbHVlbnRVSSBhbmQgdGhlbiBzYXZpbmcgaXQgYXMgYSBuZXcgZmlsZS5cclxuXHQgKiBcclxuXHQgKiBAcGFyYW0ge0RhdGV9IHNlc3Npb25JRCAtIFVVSUQgb2YgdGhlIHRhcmdldCBzZXNzaW9uXHJcblx0ICovXHJcblx0YXN5bmMgZnVuY3Rpb24gQ3JlYXRlQmFja3VwKHNlc3Npb25JRDogbnVtYmVyKSB7XHJcblx0XHR0cnkge1xyXG5cdFx0XHRsZXQgd29ya2JlbmNoRmlsZSA9IGF3YWl0IHByb21pc2VzLnJlYWRGaWxlKFdPUktCRU5DSF9GSUxFLCAndXRmLTgnKTtcclxuXHRcdFx0bGV0IGJhY2t1cFBhdGggPSBHZXRCYWNrdXBCeVVVSUQoc2Vzc2lvbklEKTtcclxuXHJcblx0XHRcdHdvcmtiZW5jaEZpbGUgPSBTYW5pdGl6ZUhUTUwod29ya2JlbmNoRmlsZSk7XHJcblxyXG5cdFx0XHRhd2FpdCBwcm9taXNlcy53cml0ZUZpbGUoYmFja3VwUGF0aCwgd29ya2JlbmNoRmlsZSwgJ3V0Zi04JylcclxuXHJcblx0XHRcdFJlcG9ydFN0YXR1cyhgQmFja3VwIGNyZWF0ZWQ6ICR7V09SS0JFTkNIX0ZJTEV9YCk7XHJcblx0XHR9IGNhdGNoIChfXykge1xyXG5cdFx0XHRSZXBvcnRFcnJvcihfXyBhcyBzdHJpbmcpO1xyXG5cdFx0XHR2c2NvZGUud2luZG93LnNob3dJbmZvcm1hdGlvbk1lc3NhZ2UoTUVTU0FHRVMuQURNSU4pO1xyXG5cdFx0fVxyXG5cdH1cclxuXHJcblx0LyoqXHJcblx0ICogXHRcclxuXHQgKiBAcGFyYW0gYmFja3VwRmlsZSBcclxuXHQgKi9cclxuXHRhc3luYyBmdW5jdGlvbiBSZXN0b3JlQmFja3VwKGJhY2t1cEZpbGU6IHN0cmluZykge1xyXG5cdFx0dHJ5IHtcclxuXHRcdFx0aWYgKGV4aXN0c1N5bmMoYmFja3VwRmlsZSkpIHtcclxuXHRcdFx0XHRhd2FpdCBwcm9taXNlcy51bmxpbmsoV09SS0JFTkNIX0ZJTEUpO1xyXG5cdFx0XHRcdGF3YWl0IHByb21pc2VzLmNvcHlGaWxlKGJhY2t1cEZpbGUsIFdPUktCRU5DSF9GSUxFKTtcclxuXHRcdFx0fVxyXG5cdFx0fSBjYXRjaCAoX18pIHtcclxuXHRcdFx0dnNjb2RlLndpbmRvdy5zaG93SW5mb3JtYXRpb25NZXNzYWdlKE1FU1NBR0VTLkFETUlOKTtcclxuXHRcdH1cclxuXHR9XHJcblxyXG5cdGFzeW5jIGZ1bmN0aW9uIFJlbW92ZUJhY2t1cEZpbGVzKCkge1xyXG5cdFx0Y29uc3QgaXRlbXMgPSBhd2FpdCBwcm9taXNlcy5yZWFkZGlyKFdPUktCRU5DSF9ESVJFQ1RPUlkpO1xyXG5cclxuXHRcdGZvciAoY29uc3QgaXRlbSBpbiBpdGVtcykge1xyXG5cdFx0XHRpZiAoaXRlbS5lbmRzV2l0aCgnLmJhay1mdWknKSkge1xyXG5cdFx0XHRcdGF3YWl0IHByb21pc2VzLnVubGluayhwYXRoLmpvaW4oV09SS0JFTkNIX0RJUkVDVE9SWSwgaXRlbSkpO1xyXG5cdFx0XHR9XHJcblx0XHR9XHJcblx0fVxyXG5cclxuXHRhc3luYyBmdW5jdGlvbiBGaW5kQmFja3VwQnlVVUlEKGZpbGVQYXRoOiBzdHJpbmcpOiBQcm9taXNlPG51bWJlciB8IG51bGwgfCB1bmRlZmluZWQ+IHtcclxuXHRcdHRyeSB7XHJcblx0XHRcdGNvbnN0IF9jb250ZW50ID0gYXdhaXQgcHJvbWlzZXMucmVhZEZpbGUoZmlsZVBhdGgsICd1dGYtOCcpO1xyXG5cdFx0XHRjb25zdCBfbWF0Y2ggPSBfY29udGVudC5tYXRjaCgvPCEtLSBGVUktSUQgKFswLTlhLWZBLUYtXSspIC0tPi8pO1xyXG5cclxuXHRcdFx0Ly8gcmV0dXJuIF9tYXRjaCAhPSBudWxsID8gX21hdGNoWzFdIDogbnVsbDtcclxuXHRcdFx0cmV0dXJuIDA7XHJcblx0XHR9IGNhdGNoIChfXykge1xyXG5cdFx0XHRSZXBvcnRFcnJvcihfXyBhcyBzdHJpbmcpO1xyXG5cdFx0XHR2c2NvZGUud2luZG93LnNob3dJbmZvcm1hdGlvbk1lc3NhZ2UoYCR7X199YCk7XHJcblx0XHR9XHJcblx0fVxyXG5cclxuXHRmdW5jdGlvbiBHZXRCYWNrdXBCeVVVSUQoc2Vzc2lvbklEOiBudW1iZXIpOiBzdHJpbmcge1xyXG5cdFx0cmV0dXJuIHBhdGguam9pbihXT1JLQkVOQ0hfRElSRUNUT1JZLCBgd29ya2JlbmNoLiR7c2Vzc2lvbklEfS5iYWstZnVpYCk7XHJcblx0fVxyXG5cclxuXHQvKipcclxuXHQgKiBcdFJlbW92ZXMgKHdoZW4gcHJlc2VudCkgdGhlIEZsdWVudFVJIGluamVjdGlvbiBmcm9tIHRoZSB3b3JrYmVuY2ggYW5kIHJldmVydHMgaXQgYmFja1xyXG5cdCAqIFx0dG8gaXRzIG9yaWdpbmFsIHN0YXRlLlxyXG5cdCAqIFxyXG5cdCAqIFx0XHRCYXNpYyBSRUdFWCBTdHJpbmcgbW9kaWZpY2F0aW9uIGZ1bmN0aW9uLCBub3RoaW5nIHRvbyBzcGVjaWFsIGFib3V0IGl0LlxyXG5cdCAqIFxyXG5cdCAqIEBwYXJhbSB7c3RyaW5nfSBfaHRtbCBcclxuXHQgKiBAcmV0dXJucyB7c3RyaW5nfSBTYW5pdGl6ZWQgSFRNTFxyXG5cdCAqL1xyXG5cdGZ1bmN0aW9uIFNhbml0aXplSFRNTChfaHRtbDogc3RyaW5nKTogc3RyaW5nIHtcclxuXHRcdF9odG1sID0gX2h0bWwucmVwbGFjZSgvXi4qKDwhLS0gRmx1ZW50IFVJIC0tPjxzY3JpcHQgc3JjPVwiZmx1ZW50LmpzXCI+PFxcL3NjcmlwdD48IS0tIEZsdWVudCBVSSAtLT4pLipcXG4/L2dtLCAnJywpO1xyXG5cdFx0X2h0bWwgPSBfaHRtbC5yZXBsYWNlKC88IS0tIEZVSS1DU1MtU1RBUlQgLS0+W1xcc1xcU10qPzwhLS0gRlVJLUNTUy1FTkQgLS0+XFxuKi8sICcnKTtcclxuXHRcdF9odG1sID0gX2h0bWwucmVwbGFjZSgvPCEtLSBGVUkgLS0+W1xcc1xcU10qPzwhLS0gRlVJIC0tPlxcbiovLCAnJyk7XHJcblx0XHRfaHRtbCA9IF9odG1sLnJlcGxhY2UoLzwhLS0gRlVJLUlEIFtcXHctXSsgLS0+XFxuKi9nLCAnJyk7XHJcblx0XHRfaHRtbCA9IF9odG1sLnJlcGxhY2UoLzwhLS0gbWV0YS1kZWZhdWx0IC0tPi8sIGA8bWV0YSBodHRwLWVxdWl2PVwiQ29udGVudC1TZWN1cml0eS1Qb2xpY3lcIiBjb250ZW50PVwiZGVmYXVsdC1zcmMgJ25vbmUnOyBpbWctc3JjICdzZWxmJyBodHRwczogZGF0YTogYmxvYjogdnNjb2RlLXJlbW90ZS1yZXNvdXJjZTo7IG1lZGlhLXNyYyAnc2VsZic7IGZyYW1lLXNyYyAnc2VsZicgdnNjb2RlLXdlYnZpZXc6OyBvYmplY3Qtc3JjICdzZWxmJzsgc2NyaXB0LXNyYyAnc2VsZicgJ3Vuc2FmZS1ldmFsJyBibG9iOjsgc3R5bGUtc3JjICdzZWxmJyAndW5zYWZlLWlubGluZSc7IGNvbm5lY3Qtc3JjICdzZWxmJyBodHRwczogd3M6OyBmb250LXNyYyAnc2VsZicgaHR0cHM6IHZzY29kZS1yZW1vdGUtcmVzb3VyY2U6O1wiPmApO1xyXG5cdFx0X2h0bWwgPSBfaHRtbC5yZXBsYWNlKC88IS0tIG1ldGEtcmVxdWlyZSAtLT4vLCBgPG1ldGEgaHR0cC1lcXVpdj1cIkNvbnRlbnQtU2VjdXJpdHktUG9saWN5XCIgY29udGVudD1cInJlcXVpcmUtdHJ1c3RlZC10eXBlcy1mb3IgJ3NjcmlwdCc7IHRydXN0ZWQtdHlwZXMgYW1kTG9hZGVyIGNlbGxSZW5kZXJlckVkaXRvclRleHQgZGVmYXVsdFdvcmtlckZhY3RvcnkgZGlmZkVkaXRvcldpZGdldCBzdGlja3lTY3JvbGxWaWV3TGF5ZXIgZWRpdG9yR2hvc3RUZXh0IGRvbUxpbmVCcmVha3NDb21wdXRlciBlZGl0b3JWaWV3TGF5ZXIgZGlmZlJldmlldyBkb21wdXJpZnkgbm90ZWJvb2tSZW5kZXJlciBzYWZlSW5uZXJIdG1sIHN0YW5kYWxvbmVDb2xvcml6ZXIgdG9rZW5pemVUb1N0cmluZztcIj5gKTtcclxuXHJcblx0XHRyZXR1cm4gX2h0bWw7XHJcblx0fVxyXG5cclxuXHQvKipcclxuXHQgKiBcdEluamVjdCBGbHVlbnRVSSBpbnRvIHRoZSBjdXJyZW50IFdPUktCRU5DSF9GSUxFXHJcblx0ICogXHRcclxuXHQgKiBcdFx0VGhpcyBpcyBkb25lIGJ5IGdldHRpbmcgdGhlIGN1cnJlbnQgV09SS0JFTkNIX0ZJTEUgYW5kIHNhbml0aXppbmcgaXQgaW4gY2FzZVxyXG5cdCAqIFx0XHR0aGVyZSBpcyBhbHJlYWR5IGEgcHJlLWV4aXN0aW5nIGluamVjdGlvbi4gRnJvbSB0aGVyZSB3ZSByZXBsYWNlIHRoZSBcclxuXHQgKiBcdFx0c2VjdXJpdHktcG9saWN5LCBpbmplY3QgbmV3IENTUyBhbmQgSlMgYW5kIHRyeSB0byB3cml0ZSB0aGF0IHRvIHRoZSB3b3JrYmVuY2guXHJcblx0ICogXHRcdFxyXG5cdCAqIFx0XHRBZnRlciBhbGwgb2YgdGhhdCBpcyBkb25lIHdlIHJlcXVlc3QgYSByZXN0YXJ0IG9mIHRoZSB3aW5kb3cuXHJcblx0ICogXHJcblx0ICogQHBhcmFtIHtEYXRlfSBfc2Vzc2lvbklEIElEIGZvciB0aGUgZ2l2ZW4gc2Vzc2lvblxyXG5cdCAqL1xyXG5cdGFzeW5jIGZ1bmN0aW9uIEluamVjdChfc2Vzc2lvbklEOiBudW1iZXIpIHtcclxuXHRcdGNvbnN0IHVzZUFjcnlsaWMgPSB2c2NvZGUud29ya3NwYWNlLmdldENvbmZpZ3VyYXRpb24oJ2ZsdWVudHVpJykuZ2V0KCd1c2VBY3J5bGljJykgYXMgQm9vbGVhbjtcclxuXHRcdGNvbnN0IHVzZVNoYWRvd3MgPSB2c2NvZGUud29ya3NwYWNlLmdldENvbmZpZ3VyYXRpb24oJ2ZsdWVudHVpJykuZ2V0KCd1c2VTaGFkb3dzJykgYXMgQm9vbGVhbjtcclxuXHRcdGNvbnN0IHNldHRpbmdzID0gYGxldCB1c2VBY3J5bGljPSR7dXNlQWNyeWxpY307bGV0IHVzZVNoYWRvd3M9JHt1c2VTaGFkb3dzfTtgO1xyXG5cdFx0bGV0IF9odG1sID0gYXdhaXQgcHJvbWlzZXMucmVhZEZpbGUoV09SS0JFTkNIX0ZJTEUsICd1dGYtOCcpO1xyXG5cdFx0XHJcblx0XHRfaHRtbCA9IFNhbml0aXplSFRNTChfaHRtbCk7XHJcblxyXG5cdFx0X2h0bWwgPSBfaHRtbC5yZXBsYWNlKC88bWV0YS4qaHR0cC1lcXVpdj1cIkNvbnRlbnQtU2VjdXJpdHktUG9saWN5XCIuKmNvbnRlbnQ9XCJkZWZhdWx0Lio+LywgJzwhLS0gbWV0YS1kZWZhdWx0IC0tPicpO1xyXG5cdFx0X2h0bWwgPSBfaHRtbC5yZXBsYWNlKC88bWV0YS4qaHR0cC1lcXVpdj1cIkNvbnRlbnQtU2VjdXJpdHktUG9saWN5XCIuKmNvbnRlbnQ9XCJyZXF1aXJlLio+LywgJzwhLS0gbWV0YS1yZXF1aXJlIC0tPicpO1xyXG5cclxuXHRcdF9odG1sID0gX2h0bWwucmVwbGFjZSgvKDxcXC9odG1sPikvLFxyXG5cdFx0XHRgPCEtLSBGVUktSUQgJHtfc2Vzc2lvbklEfSAtLT5cXG5gICtcclxuXHRcdFx0JzwhLS0gRlVJLUNTUy1TVEFSVCAtLT5cXG4nICtcclxuXHRcdFx0YDxzdHlsZT4ke0ZsdWVudFN0eWxlLmRlZmF1bHR9PC9zdHlsZT5gICtcclxuXHRcdFx0YDxzY3JpcHQ+JHtzZXR0aW5nc30ke0ZsdWVudFNjcmlwdC5kZWZhdWx0fTwvc2NyaXB0PmAgK1xyXG5cdFx0XHQnPCEtLSBGVUktQ1NTLUVORCAtLT5cXG48L2h0bWw+JyxcclxuXHRcdCk7XHJcblxyXG5cdFx0dHJ5IHtcclxuXHRcdFx0YXdhaXQgcHJvbWlzZXMud3JpdGVGaWxlKFdPUktCRU5DSF9GSUxFLCBfaHRtbCwgJ3V0Zi04Jyk7XHJcblx0XHRcdFJlcG9ydFN0YXR1cygnRmx1ZW50VUkgSW5qZWN0ZWQgaW50byBXb3JrYmVuY2ghJyk7XHJcblx0XHRcdHZzY29kZS53aW5kb3cuc2hvd0luZm9ybWF0aW9uTWVzc2FnZShNRVNTQUdFUy5FTkFCTEVEKTtcclxuXHRcdH0gY2F0Y2ggKF9fKSB7XHJcblx0XHRcdFJlcG9ydEVycm9yKF9fIGFzIHN0cmluZyk7XHJcblx0XHRcdHZzY29kZS53aW5kb3cuc2hvd0luZm9ybWF0aW9uTWVzc2FnZSgnRXJyb3InICsgX18pO1xyXG5cdFx0fVxyXG5cdH1cclxuXHJcblxyXG5cclxuXHJcblxyXG5cclxuXHJcblxyXG5cclxuXHJcblxyXG5cdC8vIGZ1bmN0aW9uIHJlc3RhcnRWU0NvZGUoKSB7IHZzY29kZS53aW5kb3cuc2hvd0luZm9ybWF0aW9uTWVzc2FnZSB9XHJcblx0Ly8gZnVuY3Rpb24gcmVsb2FkV2luZG93KCkgeyB2c2NvZGUuY29tbWFuZHMuZXhlY3V0ZUNvbW1hbmQoJ3dvcmtiZW5jaC5hY3Rpb24ucmVsb2FkV2luZG93Jyk7IH1cclxuXHJcblx0ZnVuY3Rpb24gUmVwb3J0U3RhdHVzKG1lc3NhZ2U6IHN0cmluZykgeyBjb25zb2xlLmxvZyhgW0ZsdWVudFVJXSAtICR7bWVzc2FnZX1gKTsgfVxyXG5cdGZ1bmN0aW9uIFJlcG9ydEVycm9yKG1lc3NhZ2U6IHN0cmluZykgeyBjb25zb2xlLmVycm9yKGBbRmx1ZW50VUldIC0gJHttZXNzYWdlfWApOyB9XHJcblxyXG5cclxuXHJcblx0Y29uc3QgX2luc3RhbGwgPSB2c2NvZGUuY29tbWFuZHMucmVnaXN0ZXJDb21tYW5kKCdmbHVlbnR1aS5pbnN0YWxsJywgSW5zdGFsbCk7XHJcblx0Y29uc3QgX3VuaW5zdGFsbCA9IHZzY29kZS5jb21tYW5kcy5yZWdpc3RlckNvbW1hbmQoJ2ZsdWVudHVpLnVuaW5zdGFsbCcsIFVuaW5zdGFsbCk7XHJcblx0Y29uc3QgX3JlaW5zdGFsbCA9IHZzY29kZS5jb21tYW5kcy5yZWdpc3RlckNvbW1hbmQoJ2ZsdWVudHVpLnJlaW5zdGFsbCcsIFJlaW5zdGFsbCk7XHJcblxyXG5cdGNvbnRleHQuc3Vic2NyaXB0aW9ucy5wdXNoKF9pbnN0YWxsKTtcclxuXHRjb250ZXh0LnN1YnNjcmlwdGlvbnMucHVzaChfdW5pbnN0YWxsKTtcclxuXHRjb250ZXh0LnN1YnNjcmlwdGlvbnMucHVzaChfcmVpbnN0YWxsKTtcclxufVxyXG5cclxuZXhwb3J0IGZ1bmN0aW9uIGRlYWN0aXZhdGUoKSB7IH0iXSwKICAibWFwcGluZ3MiOiAiMGpCQUFBLElBQUFBLEVBQUEsR0FBQUMsRUFBQUQsRUFBQSxjQUFBRSxFQUFBLGVBQUFDLElBQUEsZUFBQUMsRUFBQUosR0FBQSxJQUFBSyxFQUF3QixxQkFHeEJDLEVBQXFDLGNBQ3JDQyxFQUFzQjs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztHQVdmLFNBQVNDLEVBQVNDLEVBQWtDLENBQzFEQyxFQUFhLHVCQUF1QixFQUVwQyxJQUFNQyxFQUE2QixVQUFRLFFBQVEsTUFBTSxRQUFrQixFQUNyRUMsRUFBOEIsT0FBS0QsRUFBZSxLQUFNLE1BQU0sRUFDOURFLEVBQW1DLE9BQUtELEVBQWdCLG1CQUFvQixXQUFXLEVBQ3ZGRSxFQUE4QixPQUFLRCxFQUFxQixnQkFBZ0IsRUFFeEVFLEVBQVcsQ0FDaEIsTUFBTywyRUFDUCxRQUFTLHdFQUNULFNBQVUsd0VBQ1gsRUFRQSxlQUFlQyxHQUFVLENBQ3hCLElBQUlDLEVBQWEsS0FBSyxJQUFJLEVBQzFCLE1BQU1DLEVBQWFELENBQVUsRUFDN0IsTUFBTUUsRUFBT0YsQ0FBVSxDQUN4QixDQU1BLGVBQWVHLEdBQVksQ0FDMUIsSUFBTUMsRUFBVyxNQUFNQyxFQUFpQlIsQ0FBYyxFQUN0RCxHQUFJLENBQUNPLEVBQVUsT0FFZixJQUFNRSxFQUFhQyxFQUFnQkgsQ0FBUSxFQUMzQyxNQUFNSSxFQUFjRixDQUFVLEVBQzlCLE1BQU1HLEVBQWtCLENBQ3pCLENBRUEsZUFBZUMsR0FBWSxDQUMxQixNQUFNUCxFQUFVLEVBQ2hCLE1BQU1KLEVBQVEsQ0FDZixDQVVBLGVBQWVFLEVBQWFVLEVBQW1CLENBQzlDLEdBQUksQ0FDSCxJQUFJQyxFQUFnQixNQUFNLFdBQVMsU0FBU2YsRUFBZ0IsT0FBTyxFQUMvRFMsRUFBYUMsRUFBZ0JJLENBQVMsRUFFMUNDLEVBQWdCQyxFQUFhRCxDQUFhLEVBRTFDLE1BQU0sV0FBUyxVQUFVTixFQUFZTSxFQUFlLE9BQU8sRUFFM0RuQixFQUFhLG1CQUFtQkksR0FBZ0IsQ0FDakQsT0FBU2lCLEVBQVAsQ0FDREMsRUFBWUQsQ0FBWSxFQUNqQixTQUFPLHVCQUF1QmhCLEVBQVMsS0FBSyxDQUNwRCxDQUNELENBTUEsZUFBZVUsRUFBY1EsRUFBb0IsQ0FDaEQsR0FBSSxJQUNDLGNBQVdBLENBQVUsSUFDeEIsTUFBTSxXQUFTLE9BQU9uQixDQUFjLEVBQ3BDLE1BQU0sV0FBUyxTQUFTbUIsRUFBWW5CLENBQWMsRUFFcEQsTUFBRSxDQUNNLFNBQU8sdUJBQXVCQyxFQUFTLEtBQUssQ0FDcEQsQ0FDRCxDQUVBLGVBQWVXLEdBQW9CLENBQ2xDLElBQU1RLEVBQVEsTUFBTSxXQUFTLFFBQVFyQixDQUFtQixFQUV4RCxRQUFXc0IsS0FBUUQsRUFDZEMsRUFBSyxTQUFTLFVBQVUsR0FDM0IsTUFBTSxXQUFTLE9BQVksT0FBS3RCLEVBQXFCc0IsQ0FBSSxDQUFDLENBRzdELENBRUEsZUFBZWIsRUFBaUJjLEVBQXNELENBQ3JGLEdBQUksQ0FFSCxJQUFNQyxHQURXLE1BQU0sV0FBUyxTQUFTRCxFQUFVLE9BQU8sR0FDbEMsTUFBTSxpQ0FBaUMsRUFHL0QsTUFBTyxFQUNSLE9BQVNMLEVBQVAsQ0FDREMsRUFBWUQsQ0FBWSxFQUNqQixTQUFPLHVCQUF1QixHQUFHQSxHQUFJLENBQzdDLENBQ0QsQ0FFQSxTQUFTUCxFQUFnQkksRUFBMkIsQ0FDbkQsT0FBWSxPQUFLZixFQUFxQixhQUFhZSxXQUFtQixDQUN2RSxDQVdBLFNBQVNFLEVBQWFRLEVBQXVCLENBQzVDLE9BQUFBLEVBQVFBLEVBQU0sUUFBUSxxRkFBc0YsRUFBRyxFQUMvR0EsRUFBUUEsRUFBTSxRQUFRLHdEQUF5RCxFQUFFLEVBQ2pGQSxFQUFRQSxFQUFNLFFBQVEsc0NBQXVDLEVBQUUsRUFDL0RBLEVBQVFBLEVBQU0sUUFBUSw2QkFBOEIsRUFBRSxFQUN0REEsRUFBUUEsRUFBTSxRQUFRLHdCQUF5QixxV0FBcVcsRUFDcFpBLEVBQVFBLEVBQU0sUUFBUSx3QkFBeUIsc1ZBQXNWLEVBRTlYQSxDQUNSLENBYUEsZUFBZW5CLEVBQU9GLEVBQW9CLENBQ3pDLElBQU1zQixFQUFvQixZQUFVLGlCQUFpQixVQUFVLEVBQUUsSUFBSSxZQUFZLEVBQzNFQyxFQUFvQixZQUFVLGlCQUFpQixVQUFVLEVBQUUsSUFBSSxZQUFZLEVBQzNFQyxFQUFXLGtCQUFrQkYsb0JBQTZCQyxLQUM1REYsRUFBUSxNQUFNLFdBQVMsU0FBU3hCLEVBQWdCLE9BQU8sRUFFM0R3QixFQUFRUixFQUFhUSxDQUFLLEVBRTFCQSxFQUFRQSxFQUFNLFFBQVEsbUVBQW9FLHVCQUF1QixFQUNqSEEsRUFBUUEsRUFBTSxRQUFRLG1FQUFvRSx1QkFBdUIsRUFFakhBLEVBQVFBLEVBQU0sUUFBUSxhQUNyQixlQUFlckI7QUFBQTtBQUFBLFNBRU95QixvQkFDWEQsSUFBd0JFO0FBQUEsUUFFcEMsRUFFQSxHQUFJLENBQ0gsTUFBTSxXQUFTLFVBQVU3QixFQUFnQndCLEVBQU8sT0FBTyxFQUN2RDVCLEVBQWEsbUNBQW1DLEVBQ3pDLFNBQU8sdUJBQXVCSyxFQUFTLE9BQU8sQ0FDdEQsT0FBU2dCLEVBQVAsQ0FDREMsRUFBWUQsQ0FBWSxFQUNqQixTQUFPLHVCQUF1QixRQUFVQSxDQUFFLENBQ2xELENBQ0QsQ0FlQSxTQUFTckIsRUFBYWtDLEVBQWlCLENBQUUsUUFBUSxJQUFJLGdCQUFnQkEsR0FBUyxDQUFHLENBQ2pGLFNBQVNaLEVBQVlZLEVBQWlCLENBQUUsUUFBUSxNQUFNLGdCQUFnQkEsR0FBUyxDQUFHLENBSWxGLElBQU1DLEVBQWtCLFdBQVMsZ0JBQWdCLG1CQUFvQjdCLENBQU8sRUFDdEU4QixFQUFvQixXQUFTLGdCQUFnQixxQkFBc0IxQixDQUFTLEVBQzVFMkIsRUFBb0IsV0FBUyxnQkFBZ0IscUJBQXNCcEIsQ0FBUyxFQUVsRmxCLEVBQVEsY0FBYyxLQUFLb0MsQ0FBUSxFQUNuQ3BDLEVBQVEsY0FBYyxLQUFLcUMsQ0FBVSxFQUNyQ3JDLEVBQVEsY0FBYyxLQUFLc0MsQ0FBVSxDQUN0QyxDQUVPLFNBQVNDLEdBQWEsQ0FBRSIsCiAgIm5hbWVzIjogWyJleHRlbnNpb25fZXhwb3J0cyIsICJfX2V4cG9ydCIsICJhY3RpdmF0ZSIsICJkZWFjdGl2YXRlIiwgIl9fdG9Db21tb25KUyIsICJ2c2NvZGUiLCAiaW1wb3J0X2ZzIiwgInBhdGgiLCAiYWN0aXZhdGUiLCAiY29udGV4dCIsICJSZXBvcnRTdGF0dXMiLCAiQVBQX0RJUkVDVE9SWSIsICJCQVNFX0RJUkVDVE9SWSIsICJXT1JLQkVOQ0hfRElSRUNUT1JZIiwgIldPUktCRU5DSF9GSUxFIiwgIk1FU1NBR0VTIiwgIkluc3RhbGwiLCAiX3Nlc3Npb25JRCIsICJDcmVhdGVCYWNrdXAiLCAiSW5qZWN0IiwgIlVuaW5zdGFsbCIsICJiYWNrdXBJRCIsICJGaW5kQmFja3VwQnlVVUlEIiwgImJhY2t1cFBhdGgiLCAiR2V0QmFja3VwQnlVVUlEIiwgIlJlc3RvcmVCYWNrdXAiLCAiUmVtb3ZlQmFja3VwRmlsZXMiLCAiUmVpbnN0YWxsIiwgInNlc3Npb25JRCIsICJ3b3JrYmVuY2hGaWxlIiwgIlNhbml0aXplSFRNTCIsICJfXyIsICJSZXBvcnRFcnJvciIsICJiYWNrdXBGaWxlIiwgIml0ZW1zIiwgIml0ZW0iLCAiZmlsZVBhdGgiLCAiX21hdGNoIiwgIl9odG1sIiwgInVzZUFjcnlsaWMiLCAidXNlU2hhZG93cyIsICJzZXR0aW5ncyIsICJmbHVlbnRTdHlsZV9kZWZhdWx0IiwgImZsdWVudFNjcmlwdF9kZWZhdWx0IiwgIm1lc3NhZ2UiLCAiX2luc3RhbGwiLCAiX3VuaW5zdGFsbCIsICJfcmVpbnN0YWxsIiwgImRlYWN0aXZhdGUiXQp9Cg==
