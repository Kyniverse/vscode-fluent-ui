#!/usr/bin/env node

const esbuild = require('esbuild')

esbuild.build({
    entryPoints: ["./src/extension.ts"],
    outfile: "./out/extension.js",
    external: [ "vscode" ],
    logLevel: "info",
    bundle: true,
    minify: true,
    format: "cjs",
    platform: "node",
    sourcemap: "both",
    
    //  Forcing the injected files to be loaded as text
    loader: {
        ".js": "text",
        ".css": "text"
    },

    watch: true
}).catch((__) => {
    console.log(__);
    process.exit(1);
});