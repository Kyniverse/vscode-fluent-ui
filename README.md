# Fluent-UI Readme

Based on Leandro Rodrigues approach
https://marketplace.visualstudio.com/items?itemName=leandro-rodrigues.fluent-ui-vscode

Screenshots of current status:
![Bearded Theme Monokai](./images/VSCodium_QnQYGSz4zx.png)
![Bearded Theme Blueberry](./images/VSCodium_h7jzYQkgZ6.png)
![Bearded Theme Light](./images/VSCodium_hCmPbryq8z.png)
![Bearded Theme Arc](./images/VSCodium_lJOhpqumrV.png)