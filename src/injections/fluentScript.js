(function () {
    const bodyElement = document.querySelector('body');

    let Workbench = {
        TitleBar: undefined,
        Banner: undefined,
        ActivityBar: undefined,
        SideBar: undefined,
        Editor: undefined,
        AuxiliaryBar: undefined,
        BottomPanel: undefined,
        StatusBar: undefined,

        Container: []
    };

    const bootstrapObserver = new MutationObserver(WatchBootstrap);

    function InitializeFluentUI() {
        QueryWorkbench();
        ApplyFluentStyling();
    }

    function QueryWorkbench() {
        Workbench.TitleBar      = bodyElement.querySelector('#workbench\\.parts\\.titlebar')?.parentElement;
        Workbench.Banner        = bodyElement.querySelector('#workbench\\.parts\\.banner')?.parentElement;
        Workbench.ActivityBar   = bodyElement.querySelector('#workbench\\.parts\\.activitybar')?.parentElement;
        Workbench.SideBar       = bodyElement.querySelector('#workbench\\.parts\\.sidebar')?.parentElement;
        Workbench.Editor        = bodyElement.querySelector('#workbench\\.parts\\.editor')?.parentElement;
        Workbench.AuxiliaryBar  = bodyElement.querySelector('#workbench\\.parts\\.auxiliarybar')?.parentElement;
        Workbench.BottomPanel   = bodyElement.querySelector('#workbench\\.parts\\.panel')?.parentElement;
        Workbench.StatusBar     = bodyElement.querySelector('#workbench\\.parts\\.statusbar')?.parentElement;

        Workbench.Container[0]  = Workbench.TitleBar?.parentElement;
        Workbench.Container[1]  = Workbench.Container[0]?.childNodes[2];

        Workbench.Container[2]  = Workbench.AuxiliaryBar.parentElement;
        Workbench.Container[3]  = Workbench.Container[2]?.childNodes[2];

        Workbench.Container[4]  = Workbench.Editor?.parentElement;
    }

    function ApplyFluentStyling() {
        Workbench.Container[0].classList.add('fluent-container-0');

        Workbench.TitleBar.classList.add('fluent-titlebar');
        Workbench.Banner.classList.add('fluent-banner');
        Workbench.Container[1].classList.add('fluent-container-1');
        Workbench.StatusBar.classList.add('fluent-statusbar');

        Workbench.Container[2].classList.add('fluent-container-2');

        Workbench.ActivityBar.classList.add('fluent-activitybar');
        Workbench.SideBar.classList.add('fluent-sidebar');
        Workbench.Container[3].classList.add('fluent-container-3');
        Workbench.AuxiliaryBar.classList.add('fluent-auxiliarybar');

        Workbench.Container[4].classList.add('fluent-container-4');

        Workbench.Editor.classList.add('fluent-editor');
        Workbench.BottomPanel.classList.add('fluent-bottompanel');

        if (useAcrylic) { bodyElement.classList.add('acrylic'); }
        if (useShadows) {
            Workbench.SideBar.firstChild.classList.add('fluent-shadow');
            Workbench.BottomPanel.firstChild.classList.add('fluent-shadow');
        }
    }

    function WatchBootstrap(_list, _observer) {
        for (let _mutation of _list) {
            if (_mutation.type === 'childList') {
                const vscToken = document.querySelector('.vscode-tokens-styles');

                if (vscToken) {
                    _observer.disconnect();
                    ReportStatus('Bootstrap located, initializing...');
                    InitializeFluentUI();
                }
            }
        }
    }

    function ReportStatus(_message) { console.log('[FluentUI] - ' + _message); }

    bootstrapObserver.observe(bodyElement, { childList: true });
})();