import * as vscode from 'vscode';

// 	@ts-ignore
import { existsSync, promises } from 'fs';
import * as path from 'path'

//  @ts-ignore
import * as FluentScript from './injections/fluentScript.js';

//  @ts-ignore
import * as FluentStyle from './injections/fluentStyle.css';

/**
 * @param {vscode.ExtensionContext} context
 */
export function activate(context: vscode.ExtensionContext) {
	ReportStatus('Initializing FluentUI');

	const APP_DIRECTORY: string = path.dirname(require.main?.filename as string);
	const BASE_DIRECTORY: string = path.join(APP_DIRECTORY, 'vs', 'code');
	const WORKBENCH_DIRECTORY: string = path.join(BASE_DIRECTORY, 'electron-sandbox', 'workbench');
	const WORKBENCH_FILE: string = path.join(WORKBENCH_DIRECTORY, 'workbench.html');

	const MESSAGES = {
		ADMIN: 'Please run VSC with Administrator Priviliges in order to enable FluentUI',
		ENABLED: 'FluentUI is now enabled, reloading VSC for the changes to take effect',
		DISABLED: 'FluentUI is now disabled, reloading VSC for the changes to take effect'
	};

	/**
	 * 	Installs the Workspace modification
	 * 
	 * 		This is done by first generating a Session-UUID, then creating a backup of the 
	 * 		current workspace and then injecting the requires files for adjustments.
	 */
	async function Install() {
		let _sessionID = Date.now();
		await CreateBackup(_sessionID);
		await Inject(_sessionID);
	}

	/**
	 * 	
	 * @returns 
	 */
	async function Uninstall() {
		const backupID = await FindBackupByUUID(WORKBENCH_FILE);
		if (!backupID) return;

		const backupPath = GetBackupByUUID(backupID);
		await RestoreBackup(backupPath);
		await RemoveBackupFiles();
	}

	async function Reinstall() {
		await Uninstall();
		await Install();
	}

	/**
	 * 	Creates a backup of the current workspace.
	 * 
	 * 		This is done by reading the current workbench file, stripping/sanitizing it from 
	 * 		any existing injections done by FluentUI and then saving it as a new file.
	 * 
	 * @param {Date} sessionID - UUID of the target session
	 */
	async function CreateBackup(sessionID: number) {
		try {
			let workbenchFile = await promises.readFile(WORKBENCH_FILE, 'utf-8');
			let backupPath = GetBackupByUUID(sessionID);

			workbenchFile = SanitizeHTML(workbenchFile);

			await promises.writeFile(backupPath, workbenchFile, 'utf-8')

			ReportStatus(`Backup created: ${WORKBENCH_FILE}`);
		} catch (__) {
			ReportError(__ as string);
			vscode.window.showInformationMessage(MESSAGES.ADMIN);
		}
	}

	/**
	 * 	
	 * @param backupFile 
	 */
	async function RestoreBackup(backupFile: string) {
		try {
			if (existsSync(backupFile)) {
				await promises.unlink(WORKBENCH_FILE);
				await promises.copyFile(backupFile, WORKBENCH_FILE);
			}
		} catch (__) {
			vscode.window.showInformationMessage(MESSAGES.ADMIN);
		}
	}

	async function RemoveBackupFiles() {
		const items = await promises.readdir(WORKBENCH_DIRECTORY);

		for (const item in items) {
			if (item.endsWith('.bak-fui')) {
				await promises.unlink(path.join(WORKBENCH_DIRECTORY, item));
			}
		}
	}

	async function FindBackupByUUID(filePath: string): Promise<number | null | undefined> {
		try {
			const _content = await promises.readFile(filePath, 'utf-8');
			const _match = _content.match(/<!-- FUI-ID ([0-9a-fA-F-]+) -->/);

			// return _match != null ? _match[1] : null;
			return 0;
		} catch (__) {
			ReportError(__ as string);
			vscode.window.showInformationMessage(`${__}`);
		}
	}

	function GetBackupByUUID(sessionID: number): string {
		return path.join(WORKBENCH_DIRECTORY, `workbench.${sessionID}.bak-fui`);
	}

	/**
	 * 	Removes (when present) the FluentUI injection from the workbench and reverts it back
	 * 	to its original state.
	 * 
	 * 		Basic REGEX String modification function, nothing too special about it.
	 * 
	 * @param {string} _html 
	 * @returns {string} Sanitized HTML
	 */
	function SanitizeHTML(_html: string): string {
		_html = _html.replace(/^.*(<!-- Fluent UI --><script src="fluent.js"><\/script><!-- Fluent UI -->).*\n?/gm, '',);
		_html = _html.replace(/<!-- FUI-CSS-START -->[\s\S]*?<!-- FUI-CSS-END -->\n*/, '');
		_html = _html.replace(/<!-- FUI -->[\s\S]*?<!-- FUI -->\n*/, '');
		_html = _html.replace(/<!-- FUI-ID [\w-]+ -->\n*/g, '');
		_html = _html.replace(/<!-- meta-default -->/, `<meta http-equiv="Content-Security-Policy" content="default-src 'none'; img-src 'self' https: data: blob: vscode-remote-resource:; media-src 'self'; frame-src 'self' vscode-webview:; object-src 'self'; script-src 'self' 'unsafe-eval' blob:; style-src 'self' 'unsafe-inline'; connect-src 'self' https: ws:; font-src 'self' https: vscode-remote-resource:;">`);
		_html = _html.replace(/<!-- meta-require -->/, `<meta http-equiv="Content-Security-Policy" content="require-trusted-types-for 'script'; trusted-types amdLoader cellRendererEditorText defaultWorkerFactory diffEditorWidget stickyScrollViewLayer editorGhostText domLineBreaksComputer editorViewLayer diffReview dompurify notebookRenderer safeInnerHtml standaloneColorizer tokenizeToString;">`);

		return _html;
	}

	/**
	 * 	Inject FluentUI into the current WORKBENCH_FILE
	 * 	
	 * 		This is done by getting the current WORKBENCH_FILE and sanitizing it in case
	 * 		there is already a pre-existing injection. From there we replace the 
	 * 		security-policy, inject new CSS and JS and try to write that to the workbench.
	 * 		
	 * 		After all of that is done we request a restart of the window.
	 * 
	 * @param {Date} _sessionID ID for the given session
	 */
	async function Inject(_sessionID: number) {
		const useAcrylic = vscode.workspace.getConfiguration('fluentui').get('useAcrylic') as Boolean;
		const useShadows = vscode.workspace.getConfiguration('fluentui').get('useShadows') as Boolean;
		const settings = `let useAcrylic=${useAcrylic};let useShadows=${useShadows};`;
		let _html = await promises.readFile(WORKBENCH_FILE, 'utf-8');
		
		_html = SanitizeHTML(_html);

		_html = _html.replace(/<meta.*http-equiv="Content-Security-Policy".*content="default.*>/, '<!-- meta-default -->');
		_html = _html.replace(/<meta.*http-equiv="Content-Security-Policy".*content="require.*>/, '<!-- meta-require -->');

		_html = _html.replace(/(<\/html>)/,
			`<!-- FUI-ID ${_sessionID} -->\n` +
			'<!-- FUI-CSS-START -->\n' +
			`<style>${FluentStyle.default}</style>` +
			`<script>${settings}${FluentScript.default}</script>` +
			'<!-- FUI-CSS-END -->\n</html>',
		);

		try {
			await promises.writeFile(WORKBENCH_FILE, _html, 'utf-8');
			ReportStatus('FluentUI Injected into Workbench!');
			vscode.window.showInformationMessage(MESSAGES.ENABLED);
		} catch (__) {
			ReportError(__ as string);
			vscode.window.showInformationMessage('Error' + __);
		}
	}











	// function restartVSCode() { vscode.window.showInformationMessage }
	// function reloadWindow() { vscode.commands.executeCommand('workbench.action.reloadWindow'); }

	function ReportStatus(message: string) { console.log(`[FluentUI] - ${message}`); }
	function ReportError(message: string) { console.error(`[FluentUI] - ${message}`); }



	const _install = vscode.commands.registerCommand('fluentui.install', Install);
	const _uninstall = vscode.commands.registerCommand('fluentui.uninstall', Uninstall);
	const _reinstall = vscode.commands.registerCommand('fluentui.reinstall', Reinstall);

	context.subscriptions.push(_install);
	context.subscriptions.push(_uninstall);
	context.subscriptions.push(_reinstall);
}

export function deactivate() { }